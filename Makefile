GOPATH:=$(shell go env GOPATH)

WORK_PATH ?= f:
MODULE ?= user
.PHONY: proto
proto:
	protoc --proto_path=$(WORK_PATH)/ --go_out=$(WORK_PATH)/ $(WORK_PATH)/gitee.com/jingshanccc/course/$(MODULE)/proto/dto/*.proto
	protoc --proto_path=$(WORK_PATH)/ --go_out=$(WORK_PATH)/ --micro_out=$(WORK_PATH)/ $(WORK_PATH)/gitee.com/jingshanccc/course/$(MODULE)/proto/$(MODULE)/*.proto

# make build-user || make build-gateway || make build-xxx
.PHONY: build-%
build-%:
	cd $* && \
	GOARCH=amd64 GOOS=linux go build -o $* main.go

.PHONY: dev-all
dev-all:
	# 使用pm2 启动/管理所有进程方便调试
	pm2 start course.config.js

.PHONY: dev-user
dev-user:
	cd user && \
 	go build -o user main.go && ./user

.PHONY: dev-gateway
dev-gateway:
	cd gateway && \
	go build -o gateway main.go && ./gateway

.PHONY: dev-course
dev-course:
	cd course && \
	go build -o course main.go && ./course

.PHONY: dev-file
dev-file:
	cd file && \
	go build -o file main.go && ./file


# TODO: Docker没改
.PHONY: docker
docker:
	cd $(MODULE)
	docker build . -t $(MODULE):latest

.PHONY: api
api:
	micro --api_namespace=com.chan.course --auth_namespace=com.chan.course --registry=etcd --registry_address=192.168.10.130:2379 api