module.exports = {
    apps: [
        {
            name: "gateway",
            script: "make dev-gateway",
            cwd: "."
        },
        {
            name: "course",
            script: "make dev-course",
            cwd: "."
        },
        {
            name   : "file",
            script : "make dev-file",
            cwd: "."
        },
        {
            name   : "user",
            script : "make dev-user",
            cwd: "."
        }
    ]
}
